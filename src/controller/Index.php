<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2023-01-05 15:22:24
// |@----------------------------------------------------------------------
// |@LastEditTime : 2023-02-09 12:55:50
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Index.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2023 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
namespace app\admin\controller;

use think\admin\Controller;
use think\admin\model\SystemUser;
use think\admin\model\SystemMenu;
use think\admin\service\AdminService;
use think\admin\service\MenuService;

/**
 * 后台界面入口
 * Class Index
 * @package app\admin\controller
 */
class Index extends Controller
{
    /**
     * 显示后台首页
     * @throws \ReflectionException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index()
    {
        /*! 根据运行模式刷新权限 */
        AdminService::apply($this->app->isDebug());
        /*! 判断当前用户的登录状态 */
        $this->login = AdminService::isLogin();
        /*! 菜单为空且未登录跳转到登录页 */
        if (empty(MenuService::getTree()) && empty($this->login)) {
            $this->redirect(sysuri('admin/login/index'));
        } else {
            $this->title = lang('index.title');
            $this->super = AdminService::isSuper();
            $this->fetch();
        }
    }

    /**
     * 生成面包屑
     * @throws \ReflectionException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function breadcrumb()
    {
        $crumb = $this->app->request->post('crumb');
        foreach ($crumb as $key => $val) {
            $crumb[$key] = SystemMenu::mk()->where(['id'=>$val])->value('title');
        }
        $this->success(lang('index.breadCrumbSuccess'), $crumb);
    }

    /**
     * 获取系统菜单
     * @throws \ReflectionException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getmenu()
    {
        if ($this->app->request->isPost()) $this->error(lang('common.illegalRequest'));
        $menus = MenuService::getTree();
        foreach ($menus as $key => &$menu)
        {
            if (!empty($menu['sub'])) {
                foreach ($menu['sub'] as $v) {
                    $menus[$key]['path'][] = $menu['id'] . '-' . $v['id'];
                }
            }
        }
        $this->success(lang('index.creatdMenuSuccess'), $menus);
    }
}