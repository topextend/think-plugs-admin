<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2023-01-06 18:51:23
// |@----------------------------------------------------------------------
// |@LastEditTime : 2023-02-09 13:20:10
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : zh-cn.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2023 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
return [
    // 用户
    'user' => [
        'layout_config'       => '布局实时设置',
        'layout_config_tips'  => '以下配置可实时预览',
        'nightmode'           => '黑夜模式',
        'language'            => '语言',
        'language_zh'         => '简体中文',
        'language_en'         => 'English',
        'theme_color'         => '主题颜色',
        'layout'              => '框架布局',
        'layout_default'      => '默认',
        'layout_header'       => '通栏',
        'layout_menu'         => '经典',
        'layout_dock'         => '功能坞',
        'menu_is_collapse'    => '折叠菜单',
        'layout_tags'         => '标签栏',
        'search-no-result'    => '暂无搜索结果',
        'search'              => '搜索',
        'tasks'               => '任务中心',
    ],
    // 登陆
    'login' => [
        'title'               => '系统登录',
        'slogan'              => '高性能 / 精致 / 优雅',
        'describe'            => '基于Vue3 + Element-Plus 的中后台前端解决方案。',
        'accountLogin'        => '账号登录',
        'mobileLogin'         => '手机号登录',
        'userLoginTitle'      => '系统用户登录',
        'userLoginSuccess'    => '登录系统后台成功',
        'userPlaceholder'     => '用户名 / 手机 / 邮箱',
        'userError'           => '请输入用户名',
        'userLengthError'     => '用户名不能少于4位字符!',
        'userRequireError'    => '用户名不能为空!',
        'userCheckError'      => '登录账号或密码错误，请重新输入!',
        'userStatusError'     => '账号已经被禁用，请联系管理员!',
        'PWPlaceholder'       => '请输入密码',
        'PWError'             => '请输入密码',
        'PWLengthError'       => '密码不能少于4位字符!',
        'PWRequireError'      => '登录密码不能为空!',
        'PWCheckError'        => '登录账号或密码错误，请重新输入!',
        'verifPlaceholder'    => '验证码',
        'verifError'          => '请输入验证码',
        'verifLengthError'    => '请输入正确的四位验证码!',
        'verifRequireError'   => '图形验证码不能为空!',
        'verifCheckError'     => '图形验证码验证失败，请重新输入!',
        'uniqidRequireError'  => '图形验证标识不能为空',
        'rememberMe'          => '24小时免登录',
        'signIn'              => '登录',
        'signInSuccess'       => '登录成功',
        'mobilePlaceholder'   => '手机号码',
        'mobileError'         => '请输入手机号码',
        'mobileLengthError'   => '请输入正确的手机号!',
        'smsPlaceholder'      => '短信验证码',
        'smsError'            => '请输入短信验证码',
        'smsLengthError'      => '请输入正确位数的短信验证码!',
        'smsGet'              => '获取验证码',
        'langDescribe'        => '是否确认将显示语言切换为: ',
        'langChange'          => '语言切换',
        'langChangeSuccess'   => '语言切换成功,正在重新载入页面...',
    ],
    // 退出登陆
    'out' => [
        'outLoginSuccess'     => '退出登录成功',
        'outLoginTip'         => '确认是否退出当前用户？',
        'ourtCache'           => '清除缓存会将系统初始化，包括登录状态、主题、语言设置等，是否继续？',
    ],
    // 验证码
    'captcha'=> [
        'typeRequireErr'      => '类型不能为空',
        'tokenRequireErr'     => '标识不能为空',
        'createSuccess'       => '生成验证码成功',
    ],
    // 首页
    'index' => [
        'title'               => '系统管理后台',
        'breadCrumbSuccess'   => '生成面包屑成功',
        'creatdMenuSuccess'   => '获取菜单成功',
    ],
    // 公用
    'common' => [
        'select'              => '请选择',
        'confirm'             => '确认',
        'cancel'              => '取消',
        'exit'                => '退出',
        'tip'                 => '提示',
        'accessDeniedURL'     => '禁止访问外部链接！',
    ],
];