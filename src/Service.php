<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2023-02-23 16:51:51
// |@----------------------------------------------------------------------
// |@LastEditTime : 2023-02-23 17:52:23
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 
// |@----------------------------------------------------------------------
// |@FilePath     : Service.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2023 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
namespace app\admin;

use think\admin\Plugin;

/**
 * 插件服务注册
 * Class Service
 * @package app\admin
 */
class Service extends Plugin
{
    /**
     * 定义资源目录
     * @var string
     */
    protected $appCopy = 'app/admin';

    /**
     * 定义安装包名称
     * @var string
     */
    protected $package = 'topextend/think-plugs-admin';

    /**
     * 定义插件中心菜单
     * @return array
     */
    public static function menu(): array
    {
        return [
            [
                'name' => '系统配置',
                'subs' => [
                    ['name' => '系统参数配置', 'icon' => 'setting', 'node' => 'admin/config/index'],
                    ['name' => '系统任务管理', 'icon' => 'clock', 'node' => 'admin/queue/index'],
                    ['name' => '系统日志管理', 'icon' => 'memo', 'node' => 'admin/oplog/index'],
                    ['name' => '数据字典管理', 'icon' => 'scale-to-original', 'node' => 'admin/base/index'],
                    ['name' => '系统文件管理', 'icon' => 'files', 'node' => 'admin/file/index'],
                    ['name' => '系统菜单管理', 'icon' => 'credit-card', 'node' => 'admin/menu/index'],
                ],
            ],
            [
                'name' => '权限管理',
                'subs' => [
                    ['name' => '访问权限管理', 'icon' => 'notification', 'node' => 'admin/auth/index'],
                    ['name' => '系统用户管理', 'icon' => 'user', 'node' => 'admin/user/index'],
                ],
            ],
        ];
    }
}